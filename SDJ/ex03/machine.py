import beverages
import random


class CoffeeMachine:
    def __init__(self):
        print()

    def repair(self):
        print()

    def serve(self, param):
        print()


class EmptyCup(beverages.HotBeverage):
    name = "empty cup"
    price = "0.90"

    def description(self):
        description = "An empty cup ? Give me my money back"
        return description


class BrokenMachineException(Exception):
    def __init__(self):
        print("This coffee machine has to be repaired")
