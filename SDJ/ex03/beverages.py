class HotBeverage:
    price = "0.30"
    name = "hot beverage"

    def description(self):
        description = "Just some hot water in a cup"
        return description

    def __str__(self):
        print("name: " + self.name + "\nprice: " + self.price + "\ndescription: "+self.description()+"\n")


class Coffee(HotBeverage):
    name = "Coffee"
    price = "0.40"

    def description(self):
        description = "A coffee, to stay awake"
        return description


class Tea(HotBeverage):
    name = "Tea"
    price = "0.30"

    def description(self):
        description = "Just some hot water in a cup"
        return description


class Chocolate(HotBeverage):
    name = "Chocolate"
    price = "0.50"

    def description(self):
        description = "Chocolate, sweet chocolate..."
        return description


class Cappuccino(HotBeverage):
    name = "Cappuccino"
    price = "0.45"

    def description(self):
        description = "Un po' di Italia nella tua sazza"
        return description


new = HotBeverage()
new.__str__()
cof = Coffee()
cof.__str__()
cap = Cappuccino()
cap.__str__()
cho = Chocolate()
cho.__str__()
tea = Tea()
tea.__str__()
