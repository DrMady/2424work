import sys
import re
import os
import settings


def render(param):
    with open(param) as file:
        data = file.read()

    new_data = re.sub("{name}", settings.name, data)
    new_data = re.sub("{first_name}", settings.first_name, new_data)
    new_data = re.sub("{title}", settings.title, new_data)
    new_data = re.sub("{age}", settings.age, new_data)
    new_data = re.sub("{profession}", settings.profession, new_data)

    with open(sys.argv[1].split(".")[0] + '.html', 'w') as file_html:
        file_html.write(new_data)


if len(sys.argv) == 2:
    if os.path.exists(sys.argv[1]):
        if sys.argv[1].split(".")[1] == "template":
            render(sys.argv[1])
        else:
            print("Your file is not a .template\n")
    else:
        print("Your file does not exist")
else:
    print("You have too many arguments \n")
