class Intern:
    def __init__(self, name="My name ? I'm an internet , I'm a nobody I have no name."):
        self.name = name

    def __str__(self):
        return print(self.name)

    def work(self):
        raise Exception("I'm just an intern I can't do that")

    def make_coffee(self):
        yes = Coffee()
        return print(yes.__str__())


class Coffee:
    def __str__(self):
        return "This is the worst coffee you ever tasted."


myIntern = Intern("Mark")
myIntern.__str__()
myIntern.make_coffee()
myInternNo = Intern()
myInternNo.__str__()
myInternNo.work()
